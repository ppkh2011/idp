from fastapi import APIRouter

from src.controller.version1.auth_controller import auth_router as auth_router_v1

api_router = APIRouter()

api_router.include_router(
    auth_router_v1,
    prefix="/api/v1"
    )
