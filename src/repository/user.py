from datetime import datetime
import logging

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from src.repository.db import get_session, UsersTable, UserLogTable
from src.model.models import User, UserRegistration, UserLog
from src.utilities.auth_security import get_password_hash

async def get_user_detail(username: str, session: AsyncSession) -> UsersTable:
    try:
        result = await session.execute(select(UsersTable).where(UsersTable.login_name == username))
        user = result.scalars().all()

        return user
    except Exception as ex:
        logging.exception(f"EXCEPTION: user.py->get_user(): {ex}")

async def create_new_registration(new_user: UserRegistration, registered_datetime: datetime, session: AsyncSession) -> UserLog:
    try:
        new_user = UsersTable(
            login_name = new_user.login_name,
            hashed_password = get_password_hash(new_user.password),
            roles = ['driver'],
            is_active = True
        )

        # session: AsyncSession = await get_session()
        session.add(new_user)

        new_user_log = UserLogTable(
            login_name = new_user.login_name,
            log_datetime = registered_datetime,
            log_payload = {"event": "new registration"}
        )

        session.add(new_user_log)

        await session.flush()
        await session.commit()

        registered_user = UserLog(
            login_name=new_user.login_name,
            log_datetime=registered_datetime,
            log_payload='{"event": "new registration"}'
        )

        return registered_user
    except Exception as ex:
        logging.exception(f"EXCEPTION: user.py->create_new_registration(): {ex}")
