
import logging

from confluent_kafka import Producer

from core.config import get_settings

# bootstrap_server = "172.19.29.116:29092"
bootstrap_server = get_settings().BOOT_STRAP_SERVER

p = Producer({'bootstrap.servers': bootstrap_server})


def publish_order_event(source_data):
    logging.info('\n<Producing Order Event>')
    # topic = "orders-topic"
    topic = get_settings().ORDER_TOPIC

    def delivery_report(err, msg):
        """ Called once for each message produced to indicate delivery result.
            Triggered by poll() or flush(). """
        if err is not None:
            logging.info('❌ Message delivery failed: {}'.format(err))
        else:
            logging.info('✅  📬  Message delivered: "{}" to {} [partition {}]'.format(msg.value().decode('utf-8'),msg.topic(), msg.partition()))
            

    p.produce(topic, source_data.encode('utf-8'), callback=delivery_report)
    p.poll(0)

    r=p.flush()
    if r>0:
        logging.info('❌ Message delivery failed ({} message(s) still remain, did we timeout sending perhaps?)\n'.format(r))

