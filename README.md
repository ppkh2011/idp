Stand-alone or Serverless Identity Provider API

To run the source code:

1. Clone source code
2. Create and activate virtual environment--recommended to use Python 3.9.*
3. install application packages in virtual env.- pip install -r requirements.txt
4. create app.env file in root of the project and create following variables:
```
LOCAL_ENV_STATE="local"

LOCAL_APP_NAME=IdP
LOCAL_ADMIN_EMAIL=''
LOCAL_BASE_DIR=idp
LOCAL_DATABASE_URL=sqlite://src/data/idp.db
LOCAL_TITLE=IdP Service API
LOCAL_AZURE_STORAGE_CONNECTION_STRING = ''
LOCAL_STORAGE_CONTAINER = ''
LOCAL_BOOT_STRAP_SERVER=
LOCAL_ORDER_TOPIC=''

LOCAL_PRIVATE_KEY = '<generated using the steps given below>
'

LOCAL_PUBLIC_KEY = '<generated using the steps given below>
'

LOCAL_PUBLIC_KEY_ACTIVE = True
LOCAL_ALGORITHM = 'RS256'
LOCAL_ACCESS_TOKEN_EXPIRE_MINUTES = 30
LOCAL_AUDIENCE = 'IdP:auth'

LOCAL_HOST_SERVER = ''
LOCAL_HOST_SERVER_PORT = 0
LOCAL_DATABASE_NAME = ''
LOCAL_DATABASE_USER_NAME = ''
LOCAL_DATABASE_USER_PASSWORD = ''
LOCAL_SSL_MODE = ''

LOCAL_ECHO_SQL = False

LOCAL_DB_CONNECTION_STRING = ""
```
4. generate private key, public using following code and add them to LOCAL_PRIVATE_KEY and LOCAL_PUBLIC_KEY variables in app.env file
```
import os
import codecs
import dotenv
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend

def generate_rsa_keypair():
    env_path = os.path.join(os.path.dirname(__file__), 'app.env')
    key = rsa.generate_private_key(backend=default_backend(), public_exponent=65537, key_size=2048)

    public_key = key.public_key().public_bytes(serialization.Encoding.OpenSSH, serialization.PublicFormat.OpenSSH)

    pem = key.private_bytes(encoding=serialization.Encoding.PEM, format=serialization.PrivateFormat.TraditionalOpenSSL, encryption_algorithm=serialization.NoEncryption())

    private_key_str = pem.decode('utf-8')
    public_key_str = public_key.decode('utf-8')
    modulus = codecs.encode(codecs.decode(hex(key.public_key().public_numbers().n)[2:], 'hex'), 'base64').decode()

    dotenv.set_key(env_path, 'LOCAL_PRIVATE_KEY', private_key_str)
    dotenv.set_key(env_path, 'LOCAL_PUBLIC_KEY', public_key_str)

    print('\n///////////////// Keys generated and added to .env file.\n')
```
5. Open app.env file to check if key values are set properly
6. start application by running: python main.py
================================================================
To build your own docker and run it (assumes docker is installed):
1. Clone source code
2. create app.env file per steps 4, 5 above
3. Build docker image using docker build -t idp:0.0.1 .
4. run docker using docker run -p 6061:6061 --name idp-container001
================================================================
Here’s how to use the IdP APIs:

•	First: Use API https://localhost:6061/api/v1/register to create login for yourself or any team member.  Use any login name, password to your liking. You need to provide a registration code.  Currently, it is set to IdP:auth
•	Second: Use API https://localhost:6061/api/v1/login using previously registered credentials to login.  On successful login, you will get JWT as part of the response data. This token needs to be used for making other API and other microservice calls
•	Third: Use API https://localhost:6061/api/v1/pbkey to get public key corresponding to the token.  This key can be used to decode JWT, infer roles (scopes), etc. of the logged-in user in Microservices and other APIs to perform Authorization i.e., allow or deny the request
•	Fourth: Use API https://localhost:6061/api/v1/users/me after setting in Authorization attribute with Bearer <JWT> in request to get your own registered data.  Postman will make it easier to set this Authorization attribute
•	Finally: Use API https://localhost:6061/api/v1/logout to logout.  This will invalidate the JWT.
