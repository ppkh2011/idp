import logging
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

from jose import JWTError, jwt
from passlib.context import CryptContext


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

def get_password_hash(password):
    return pwd_context.hash(password)

def create_access_token(token_data_to_encode: dict, private_key: str, algorithm: str):
    try:
        # private_key = serialization.load_pem_private_key(secret_key.encode(), password=None, backend=default_backend())
        encoded_jwt = jwt.encode(token_data_to_encode, private_key, algorithm)

        return encoded_jwt
    except Exception as ex:
        logging.exception(f"EXCEPPTION-auth_security->create_access_token(): {ex}")
        raise ex

def decode_access_token(token_data_to_decode: dict, public_key: str, algorithm: str, audience: str):
    try:
        # public_key = serialization.load_pem_public_key(secret_key.encode(), backend=default_backend())        
        payload = jwt.decode(token_data_to_decode, public_key, algorithms=[algorithm], audience=audience)
        
        return payload
    except Exception as ex:
        logging.exception(f"EXCEPTION: auth_security->decode_access_token(): {ex}")
        raise ex
    