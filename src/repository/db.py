# from typing import List
from array import array
import asyncio
from multiprocessing.dummy import Array
import string

from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import func
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import JSON
from sqlalchemy import Boolean
from sqlalchemy import PickleType
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.ext.mutable import MutableList
from sqlalchemy.future import select
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import selectinload
from sqlalchemy.orm import sessionmaker

# from sqlalchemy.dialects.postgresql import ARRAY, JSONB, array

from core import config

config_settings = config.get_settings()

host_server = config_settings.HOST_SERVER
host_server_port = config_settings.HOST_SERVER_PORT
database_name = config_settings.DATABASE_NAME
db_username = config_settings.DATABASE_USER_NAME
db_password = config_settings.DATABASE_USER_PASSWORD
ssl_mode = config_settings.SSL_MODE
echo_sql = config_settings.ECHO_SQL

# DATABASE_URL = f"postgresql+asyncpg://{db_username}:{db_password}@{host_server}:{host_server_port}/{database_name}?ssl={ssl_mode}"
DATABASE_URL = "sqlite+aiosqlite:///src/data/idp.db"

Base = declarative_base()
engine = create_async_engine(DATABASE_URL, echo=echo_sql, future=True)

class UsersTable(Base):
    __tablename__ = "tbl_users"

    login_name = Column(String, primary_key=True)
    hashed_password = Column(String, nullable=False)
    roles = Column(MutableList.as_mutable(PickleType), nullable=False, default=["guest"])
    is_active = Column(Boolean, nullable=False, default=1)

    # required in order to access columns with server defaults
    # or SQL expression defaults, subsequent to a flush, without
    # triggering an expired load
    __mapper_args__ = {"eager_defaults": True}

class UserLogTable(Base):
    __tablename__ = "tbl_userlog"

    login_name = Column(String, primary_key=True)
    log_datetime = Column(DateTime, primary_key=True)
    log_payload = Column(JSON, nullable=False)

class InvalidTokensTable(Base):
    __tablename__ = "tbl_invalid_tokens"

    token = Column(String, primary_key=True)
    token_logout_datetime = Column(DateTime, nullable=False)
    token_logout_source = Column(String, nullable=False)

async def init_db():
    # engine = create_async_engine(DATABASE_URL, echo=True, future=True)
    
    async with engine.begin() as conn:
        # await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

    # expire_on_commit=False will prevent attributes from being expired after commit.
    # async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)

async def get_session() -> AsyncSession:
    async_session = sessionmaker(
        engine, class_=AsyncSession, expire_on_commit=False
    )
    async with async_session() as session:
        yield session


# database = databases.Database(DATABASE_URL)
# metadata = sqlalchemy.MetaData()

# #postgresql
# engine = create_async_engine(DATABASE_URL, pool_size=3, max_overflow=0, echo=True, future=True)

# # engine = create_async_engine(DATABASE_URL, echo=echo_sql, future=True)
# async def init_db():
#     async with engine.begin() as conn:
#         # await conn.run_sync(SQLModel.metadata.drop_all)
#         await conn.run_sync(SQLModel.metadata.create_all)


# async def get_session() -> AsyncSession:
#     async_session = sessionmaker(
#         engine, class_=AsyncSession, expire_on_commit=False, class_=AsyncSession
#     )
#     async with async_session() as session:
#         yield session


# # users = sqlalchemy.Table(
# #     "users",
# #     metadata,
# #     sqlalchemy.Column("login_name", sqlalchemy.String, primary_key=True),
# #     sqlalchemy.Column("hashed_password", sqlalchemy.String, nullable=False),
# #     sqlalchemy.Column("roles", sqlalchemy.ARRAY(sqlalchemy.String), nullable=False),
# #     sqlalchemy.Column("is_active", sqlalchemy.Boolean, nullable=False)
# # )

# # user_log = sqlalchemy.Table(
# #     "user_log",
# #     metadata,
# #     sqlalchemy.Column("login_name", sqlalchemy.String, primary_key=True),
# #     sqlalchemy.Column("log_datetime", sqlalchemy.DateTime, nullable=True),
# #     sqlalchemy.Column("log_payload", postgresql.JSONB, nullable=False)
# # )

# invalid_tokens = sqlalchemy.Table(
#     "invalid_tokens",
#     metadata,
#     sqlalchemy.Column("token", sqlalchemy.String, primary_key=True),
#     sqlalchemy.Column("logout_datetime", sqlalchemy.DateTime, nullable=False),
#     sqlalchemy.Column("logout_source", sqlalchemy.String, nullable=True)
# )

#sqlite
# engine = sqlalchemy.create_engine(
#     DATABASE_URL, connect_args={"check_same_thread": False}
# )
