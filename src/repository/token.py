from datetime import datetime
import logging

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from src.model.models import InvalidToken
from src.repository.db import InvalidTokensTable, get_session

async def get_invalid_token(token: str, session: AsyncSession):
    try:
        result = await session.execute(select(InvalidTokensTable).where(InvalidTokensTable.token == token))
        invalid_token = result.scalars().all()

        return invalid_token
    except Exception as ex:
        logging.exception(f"EXCEPTION: token.py->get_invalid_token(): {ex}")

async def put_invalid_token(token: str, logout_datetime: datetime, logout_source: str, session: AsyncSession) -> InvalidToken:
    try:
        new_invalid_token = InvalidTokensTable(
            token = token,
            token_logout_datetime = logout_datetime,
            token_logout_source = logout_source
        )

        session.add(new_invalid_token)

        await session.flush()
        await session.commit()

        return True
    except Exception as ex:
        logging.exception(f"EXCEPTION: token.py->put_invalid_token(): {ex}")
