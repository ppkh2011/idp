from functools import lru_cache
from typing import Optional

from pydantic import BaseSettings, Field, BaseModel


class AppConfig(BaseModel):
    """Application configurations."""

    VERSION: float = 0.1
    NAME: str = "New Order Processor Function API"


class GlobalConfig(BaseSettings):
    """Global configurations."""

    # These variables will be loaded from the .env file. However, if
    # there is a shell environment variable having the same name,
    # that will take precedence.

    APP_CONFIG: AppConfig = AppConfig()

    # define global variables with the Field class
    # ENV_STATE: Optional[str] = Field(None, env="local")     #to be setup thru OS environment variable
    ENV_STATE = "local"     #to be setup thru OS environment variable

    APP_NAME: str = None
    ADMIN_EMAIL: str = None
    BASE_DIR: str = None
    DATABASE_URL: str = None
    AZURE_STORAGE_CONNECTION_STRING: str = None
    STORAGE_CONTAINER: str = None
    BOOT_STRAP_SERVER: str = None
    ORDER_TOPIC: str = None

    PRIVATE_KEY: str = None
    PUBLIC_KEY: str = None
    ALGORITHM: str = None
    ACCESS_TOKEN_EXPIRE_MINUTES: int = None
    AUDIENCE: str = None
    PUBLIC_KEY_ACTIVE: bool = False

    HOST_SERVER: str = None
    HOST_SERVER_PORT: int = None
    DATABASE_NAME: str = None
    DATABASE_USER_NAME: str = None
    DATABASE_USER_PASSWORD: str = None
    SSL_MODE: str = None

    ECHO_SQL: bool = False

    class Config:
        """Loads the dotenv file."""

        env_file: str = "app.env"


class LocalConfig(GlobalConfig):
    """Local Development configurations."""

    class Config:
        env_prefix: str = "LOCAL_"


class RemoteConfig(GlobalConfig):
    """Remote Development configurations."""

    class Config:
        env_prefix: str = "REMOTE_"

class TestConfig(GlobalConfig):
    """Test configurations."""

    class Config:
        env_prefix: str = "TEST_"

class ProdConfig(GlobalConfig):
    """Production configurations."""

    class Config:
        env_prefix: str = "PROD_"


class FactoryConfig:
    """Returns a config instance dependending on the ENV_STATE variable."""

    def __init__(self, env_state: Optional[str]):
        self.env_state = env_state

    def __call__(self):
        if self.env_state == "local":
            return LocalConfig()

        elif self.env_state == "remote":
            return RemoteConfig()

        elif self.env_state == "test":
            return TestConfig()

        elif self.env_state == "prod":
            return ProdConfig()


# settings = FactoryConfig(GlobalConfig().ENV_STATE)()
# print(settings.__repr__())

@lru_cache()
def get_settings():
    settings = FactoryConfig(GlobalConfig().ENV_STATE)()
    return settings

config_settings = get_settings()