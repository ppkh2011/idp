from datetime import datetime
from typing import List, Union

from pydantic import BaseModel, Json

class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: Union[str, None] = None
    scopes: List[str] = []

class User(BaseModel):
    login_name: str
    hashed_password: str
    roles: List[str]
    is_active: bool

    class Config:
        orm_mode = True

class UserLog(BaseModel):
    login_name: str
    log_datetime: datetime
    log_payload: Json

class UserInDB(User):
    hashed_password: str
    roles: List[str]
    active: bool

class InvalidToken(BaseModel):
    token: str
    logout_datetime: datetime
    logout_source: str

class UserRegistration(BaseModel):
    login_name: str
    password: str
    registration_code: str

class PublicKey(BaseModel):
    is_valid: bool
    public_key: str