import logging
from logging.handlers import RotatingFileHandler

from fastapi import FastAPI
from fastapi.openapi.docs import (
    get_redoc_html,
    get_swagger_ui_html,
    get_swagger_ui_oauth2_redirect_html,
)
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
import uvicorn

from src.routes import api_router
from src.repository import db

def __init_logging():
    logging.basicConfig(
        # handlers=[RotatingFileHandler('log/nwordapi.log', maxBytes=100000, backupCount=10)],
        level=logging.WARNING,
        format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
        datefmt='%Y-%m-%dT%H:%M:%S:%s %Z%z')

def __init_routes(app: FastAPI):
    logging.info("initializing all routes")
    
    app.include_router(
        router=api_router,
        tags=["API Routes"],
    )

def __init_application():
    __init_logging()

    logging.info("creating API service and Web Site")

    app = FastAPI(
        title="ALMP-IdP API Service",
        version="0.1",
        docs_url=None,
        redoc_url=None
    )

    app.mount("/static", StaticFiles(directory="src/static"), name="static")
    
    __init_routes(app)

    return app

api_app = __init_application()

api_app.add_middleware(
    CORSMiddleware,
    allow_origins=["localhost:*", "*.americana.com"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

@api_app.on_event("startup")
async def startup():
    # api_app.state.engine = await db.init_db()
    await db.init_db()

@api_app.on_event("shutdown")
async def shutdown():
    # for AsyncEngine created in function scope, close and
    # clean-up pooled connections
    pass

@api_app.get("/docs", include_in_schema=False)
async def custom_swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url=api_app.openapi_url,
        title=api_app.title + " - Swagger UI",
        oauth2_redirect_url=api_app.swagger_ui_oauth2_redirect_url,
        swagger_js_url="/static/swagger-ui-bundle.js",
        swagger_css_url="/static/swagger-ui.css",
    )


@api_app.get(api_app.swagger_ui_oauth2_redirect_url, include_in_schema=False)
async def swagger_ui_redirect():
    return get_swagger_ui_oauth2_redirect_html()


@api_app.get("/redoc", include_in_schema=False)
async def redoc_html():
    return get_redoc_html(
        openapi_url=api_app.openapi_url,
        title=api_app.title + " - ReDoc",
        redoc_js_url="/static/redoc.standalone.js",
    )

if __name__ == "__main__":
    uvicorn.run(
        app="main:api_app",
        host="0.0.0.0",
        port=9091,
        reload=True
    )