from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, status, Request, Header
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer
from sqlalchemy.ext.asyncio import AsyncSession

from src.model.models import Token, User, UserRegistration, PublicKey
from src.service.auth_service import authenticate_user, generate_access_token, get_current_active_user, logout_current_active_user, register_user, get_public_key
from src.repository.db import get_session

auth_router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="api/v1/login",
    scopes={"me": "Get information about you"}
    )

@auth_router.post("/login", response_model=Token)
async def login(form_data: OAuth2PasswordRequestForm = Depends(), session: AsyncSession = Depends(get_session)):
    user = await authenticate_user(form_data.username, form_data.password, session)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    
    access_token = generate_access_token(user)
    return {"access_token": access_token, "token_type": "bearer"}


@auth_router.post("/logout")
async def logout(request: Request, current_user: User = Depends(get_current_active_user), token: str = Depends(oauth2_scheme), session: AsyncSession = Depends(get_session), x_forwarded_for: Optional[str] = Header(None)):
    if x_forwarded_for is None:
        ip_address = request.client.host
    else:
        ip_address = x_forwarded_for

    user = await logout_current_active_user(token, ip_address, session)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return {"message": "logout completed"}

@auth_router.get("/users/me/", response_model=User)
async def read_users_me(current_user: User = Depends(get_current_active_user)):
    return current_user

@auth_router.post("/register", status_code=201)
async def register(user_registration: UserRegistration, session: AsyncSession = Depends(get_session)):
    new_user = await register_user(user_registration, session)

    return new_user

@auth_router.get("/pbkey/", response_model=PublicKey)
async def read_public_key():
    public_key_response: PublicKey = await get_public_key()
    return public_key_response