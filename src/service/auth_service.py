from datetime import datetime, timedelta
import logging

#TODO: remove all fastapi references and replace with application exception and return http exceptions only from controllers
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError
from sqlalchemy.ext.asyncio import AsyncSession

from core.config import config_settings
from src.model.models import User, TokenData, UserLog, UserRegistration, PublicKey
from src.repository.db import get_session
from src.repository.user import get_user_detail, create_new_registration
from src.repository.token import get_invalid_token, put_invalid_token
from src.utilities.auth_security import verify_password, create_access_token, decode_access_token

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="api/v1/login",
    scopes={"me": "Get information about you"}
    )

async def authenticate_user(username: str, password: str, session: AsyncSession):
    try:
        user_rec = await get_user_detail(username, session)
        if user_rec is None:
            return False
        if not verify_password(password, user_rec[0].hashed_password):
            return False
        return user_rec
    except Exception as ex:
        logging.critical(f"EXCEPTION-auth_service:authenticate_user(): {ex}")

def generate_access_token(user_data: User):
    # SECRET_KEY = "9a582f8176f2a3e233daf3145f9de698c6e23df4be4ed2c8c288f85c2798bd4f067cef52ab09d85c2987edeb5d33de6052dede33b223eb3f524b09ab70be4106"
    SECRET_KEY = config_settings.PRIVATE_KEY
    # ALGORITHM = "HS256"
    ALGORITHM = config_settings.ALGORITHM
    # ACCESS_TOKEN_EXPIRE_MINUTES = 30
    ACCESS_TOKEN_EXPIRE_MINUTES = config_settings.ACCESS_TOKEN_EXPIRE_MINUTES

    current_datetime = datetime.utcnow()

    token_expire_time = current_datetime + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    
    token_data_to_encode = {
        "sub": user_data[0].login_name,
        "exp": token_expire_time,
        'iat' : current_datetime,
        "scopes": {
            "me": "Get information about you",
            "roles": user_data[0].roles
        },
        "aud": config_settings.AUDIENCE,  #URI of the server hositng IdP service
    }

    encoded_jwt = create_access_token(
        token_data_to_encode=token_data_to_encode,
        private_key=SECRET_KEY,
        algorithm=ALGORITHM
    )

    return encoded_jwt

async def get_current_user(session: AsyncSession = Depends(get_session), token: str = Depends(oauth2_scheme)):
    credentials_exception: HTTPException = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:    
        token_rec = await get_invalid_token(token, session)
        if len(token_rec) > 0:
            credentials_exception.detail = "Invalid or Expired token"
            raise credentials_exception

         # SECRET_KEY = "9a582f8176f2a3e233daf3145f9de698c6e23df4be4ed2c8c288f85c2798bd4f067cef52ab09d85c2987edeb5d33de6052dede33b223eb3f524b09ab70be4106"
        SECRET_KEY = config_settings.PUBLIC_KEY
        # ALGORITHM = "HS256"
        ALGORITHM = config_settings.ALGORITHM
        # AUDIENCE = "almp_IdP:auth"
        AUDIENCE = config_settings.AUDIENCE

        payload = decode_access_token(token_data_to_decode=token, public_key=SECRET_KEY, algorithm=ALGORITHM, audience=AUDIENCE)

        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception

        token_data = TokenData(username=username)
    except JWTError as ex:
        logging.exception("EXCEPTION-auth_service.py:get_current_user(): {ex}")
        raise credentials_exception

    user_rec = await get_user_detail(username=token_data.username, session=session)
    if len(user_rec) == 0:
        raise credentials_exception
    
    return user_rec[0]

async def get_current_active_user(current_user: User = Depends(get_current_user)):
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user

async def logout_current_active_user(token: str, ip_address: str, session: AsyncSession):
    try:
        user_rec = await put_invalid_token(token, datetime.now(), ip_address, session)
        if not user_rec:
            return False
        return True
    except Exception as ex:
        logging.critical(f"EXCEPTION-auth_service:logout_user(): {ex}")

async def register_user(new_user: UserRegistration, session: AsyncSession) -> UserLog:
    try:
        if new_user.registration_code != config_settings.AUDIENCE:
            raise HTTPException(status_code=401, detail="Invalid Registration code")

        registered_datetime = datetime.now()
        user_rec = await create_new_registration(new_user, registered_datetime, session)
        if not user_rec:
           raise HTTPException(status_code=422, detail="Unable to complete registration")
        return user_rec
    except Exception as ex:
        logging.critical(f"EXCEPTION-auth_service:register_user(): {ex}")

async def get_public_key():
    public_key = PublicKey(is_valid=config_settings.PUBLIC_KEY_ACTIVE, public_key=config_settings.PUBLIC_KEY)
    return public_key